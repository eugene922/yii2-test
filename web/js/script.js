$(document).ready(function(){;
    $('#review-form button').click(function($e){

        $e.preventDefault();

        $.ajax({
            type: "POST",
            url: '/review/store',
            data: $('#review-form').serialize(),
            success: function(data){
                if (data.status == 'ok') {
                    $('#review-was-added').show();

                    setTimeout(function(){
                        $('#review-was-added').hide();
                    }, 3000);

                    var review = $('.review:first').clone();

                    $(review).find('.review-name').text(data.review.name);
                    $(review).find('.review-message').text(data.review.message);
                    $(review).find('.review-date').text(data.review.date);

                    $('.review:first').after(review);
                } else {
                    alert('Some error');
                    console.log(data);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error: '.textStatus);
            }
        });
    })

})