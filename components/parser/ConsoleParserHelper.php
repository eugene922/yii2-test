<?php


namespace app\components\parser;

use yii\db\Migration;

class ConsoleParserHelper extends ParserHelper
{
    protected $migration_instanse;

    public function setMigrationInstanse(Migration $migration_instanse)
    {
        $this->migration_instanse = $migration_instanse;

        return $this;
    }

    protected function insert($sql, $columns)
    {
        $this->migration_instanse->insert($sql, $columns);
    }

}