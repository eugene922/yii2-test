<?php


namespace app\components\parser;

use yii\db\Connection;

class WebParserHelper extends ParserHelper
{
    protected $connection;

    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;

        return $this;
    }

    protected function insert($table, $columns)
    {
        $this->connection->createCommand()->insert($table, $columns)->execute();
    }

}