<?php

namespace app\components\parser;

use yii\base\ErrorException;
use yii\db\Query;

abstract class ParserHelper
{
    protected $filename;

    public function setFile($filename)
    {
        if (!file_exists($filename)){
            throw new ErrorException("File don't exists");
        }

        $this->filename = $filename;

        return $this;
    }

    public function run()
    {

        # Init DOMDocument lib for parsing data
        $dom = new \DOMDocument();
        $dom->load($this->filename);
        $products = $dom->getElementsByTagName('product');

        # Get description of ratings from the table
        $dataRatings = (new Query)
            ->select(['id', 'name', 'description'])
            ->from('rating_description')
            ->all();
        if (empty($dataRatings))
            throw new ErrorException('Empty `rating_description` table');

        $ratings = [];
        foreach($dataRatings as $dataRating) {
            $ratings[$dataRating['name']] = $dataRating['id'];
        }

        $last_product_id = 0;

        foreach($products as $product){

            $node_product_id = $product->getElementsByTagName('product_id');

            # the existence check product_id node
            if (!empty($node_product_id)) {
                $product_id = $last_product_id = $node_product_id->item(0)->nodeValue;
            } else {
                $product_id = $last_product_id;
            }

            $title = $product->getElementsByTagName('title')->item(0)->nodeValue;
            $description = $product->getElementsByTagName('description')->item(0)->nodeValue;
            $price = $product->getElementsByTagName('price')->item(0)->nodeValue;
            $rating = $product->getElementsByTagName('rating')->item(0)->nodeValue;
            $image = $product->getElementsByTagName('image')->item(0)->nodeValue;

            # the existence check inet_price node
            $node_inet_price = $product->getElementsByTagName('inet_price');
            if (!empty($node_inet_price)) {
                $inet_price = $node_inet_price->item(0)->nodeValue;
            } else {
                $inet_price = null;
            }

            if ($rating <= 3 ) {
                $rating_description_id = $ratings['плохой'];
            } elseif ($rating > 3 && $rating <= 4) {
                $rating_description_id = $ratings['хороший'];
            } elseif ($rating > 4 && $rating <= 5) {
                $rating_description_id = $ratings['отличный'];
            } else {
                throw new ErrorException("Not valid rating of the product: 'id'-'$product_id', 'title'-'$title', 'rating'-$rating");
            }

            $this->insert('products', compact('product_id', 'title', 'description', 'price', 'inet_price', 'rating', 'rating_description_id', 'image'));

        }

        return true;

    }

    abstract protected function insert($table, $data);

}