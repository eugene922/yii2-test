<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use Carbon\Carbon;

$this->title = Html::encode($product->title);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-contact">
    <div class="row">
        <h1>#<?php echo $product->product_id ?> - <?php echo $product->title ?></h1>
        <div class="col-md-5"><img src="<?php echo $product->image ?>" alt=""></div>
        <div class="col-md-7">
            <div class="row">

                <h2>Description</h2>
                <p><?php echo $product->description ?></p>
            </div>
            <div class="row">
                <dl>
                    <dt>Price</dt>
                    <dd> <?php echo $product->price ?></dd>
                    <dt>Rating</dt>
                    <dd> <?php echo $product->rating ?>(<?php echo ($product->ratingDescription->name) ?>)</dd>
                </dl>
            </div>
        </div>
    </div>

    <h2>Send review</h2>

    <div id="review-was-added" class="alert alert-success" style="display:none">
        Thank you for your review of the product..
    </div>

        <p>
            If you have business inquiries or other questions, please fill out the following form to contact us.
            Thank you.
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'review-form']); ?>

                <?= $form->field($reviewForm, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($reviewForm, 'email') ?>

                <?= $form->field($reviewForm, 'message')->textarea() ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <input type="hidden" name="product_id" value="<?php echo $product->id ?>">
                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <div class="row">
        <h3>Reviews</h3>
        <div class="review">
            <div class="col-md-2 review-name"></div>
            <div class="col-md-8 review-message"></div>
            <div class="col-md-2 review-date"></div>
        </div>
        <?php foreach($reviews as $review): ?>
        <div class="review">
            <div class="col-md-2 review-name"><?php echo $review->name; ?></div>
            <div class="col-md-8 review-message"><?php echo $review->message; ?></div>
            <div class="col-md-2 review-date">
                <?php
                $date_review = new Carbon();
                $date_review->timestamp = $review->created_at;
                echo $date_review->diffForHumans() ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

