<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">Список товаров.</p>
        <?php if (Yii::$app->session->hasFlash('fileWasUploaded')): ?>
        <div class="alert alert-success">
            file has been successfully uploaded
        </div>
        <?php endif; ?>


    </div>
    <div class="row">

        <?php $form = ActiveForm::begin(['id' => 'upload-form', 'action' => Url::to(['product/upload']), 'options' => ['enctype' => 'multipart/form-data']]) ?>

        <div class="form-group">
            <?= $form->field($uploadForm, 'dataFile')->fileInput() ?>
            <p class="help-block">Send your file with xml data.</p>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="body-content">

        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Краткой описание</th>
                        <th>Цена</th>
                        <th>Рэйтинг</th>
                        <th>Изображение</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($products as $product): ?>
                        <tr>
                            <td><a href="<?php echo Url::to(['product/show', 'id' => $product->id]); ?>"><?php echo $product->product_id ?></a></td>
                            <td><?php echo $product->title ?></td>
                            <td><?php echo mb_substr($product->description, 0, 150) ?>...</td>
                            <td><?php echo $product->price ?></td>
                            <td><?php echo $product->rating ?></td>
                            <td><a href="<?php echo Url::to(['product/show', 'id' => $product->id]); ?>"><img src="<?php echo $product->image ?>" alt="<?php echo $product->title ?>"></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo LinkPager::widget(['pagination' => $pagination]) ?>
        </div>

    </div>
</div>
