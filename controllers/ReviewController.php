<?php

namespace app\controllers;

use app\models\ReviewForm;
use yii\base\ErrorException;
use yii\web\Controller;
use app\models\Products;
use app\models\Review;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;
use yii\filters\ContentNegotiator;
use Yii;
use yii\web\Response;
use Carbon\Carbon;

class ReviewController extends Controller
{
    public function behaviors()
    {
        return [
            [
            'class' => ContentNegotiator::className(),
            'only' => ['store'],
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    public function actionStore()
    {
        $review = new Review;

        $product = Products::findOne(Yii::$app->request->post('product_id'));

        if (!$product){
            throw new NotFoundHttpException('The Product was not found');
        }

        if ($review->load(Yii::$app->request->post(), 'ReviewForm')) {

            $isValid = $review->validate();

            if ($isValid) {
                $review->browser = Yii::$app->request->userAgent;
                $review->ip = Yii::$app->request->userIP;
                $review->product_id = $product->id;

                $review->save(false);

                $reviewResponse = [
                    'name' => $review->name,
                    'message' => $review->message,
                    'date' => Carbon::now()->diffForHumans()
                ];

                return [
                    'status' => 'ok',
                    'review' => $reviewResponse
                ];
            }
        } else {
            return $review->getErrors();
        }

        return "The review was not added";
    }
}