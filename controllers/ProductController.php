<?php

namespace app\controllers;

use yii\base\ErrorException;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Products;
use yii\web\NotFoundHttpException;
use app\components\parser\WebParserHelper;
use yii\db\Connection;
use yii\web\UploadedFile;
use app\models\Review;
use app\models\ReviewForm;
use app\models\UploadForm;
use Yii;

class ProductController extends Controller
{
    public function actionShow($id)
    {
        $product = Products::findOne($id);

        if (empty($product)){
            throw new NotFoundHttpException();
        }

        $reviews = $product->reviews;

        $reviewForm = new ReviewForm;

        return $this->render('/site/product/show.php', [
            'product' => $product,
            'reviews' => $reviews,
            'reviewForm' => $reviewForm,
        ]);
    }

    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->dataFile = UploadedFile::getInstance($model, 'dataFile');

            if ($model->validate()) {
                $connection = Yii::$app->getDb();
                $transaction = $connection->beginTransaction();
                try {
                    $connection->createCommand()->truncateTable('products')->execute();

                    $parser = new WebParserHelper();

                    $parser
                        ->setConnection($connection)
                        ->setFile($model->dataFile->tempName)
                        ->run();
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }

                Yii::$app->session->setFlash('fileWasUploaded');
                return $this->redirect(Url::to(['site/index']));
            }
        }
    }
}