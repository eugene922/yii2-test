<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Review extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'max' => 255],
            ['name', 'match', 'pattern' => '/^[a-zA-Z]+$/i'],
            ['message', 'string', 'max' => 2550],
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className, ['id' => 'product_id'])->orderBy('created_at DESC');;
    }

}
