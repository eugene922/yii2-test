<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ReviewForm extends Model
{
    public $name;
    public $email;
    public $message;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name', 'email', 'message'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'max' => 255],
            ['name', 'match', 'pattern' => '/^[a-zA-Z]\w*$/i'],
            ['message', 'string', 'max' => 2550],
//            ['verifyCode', 'captcha'],
        ];
    }


}
