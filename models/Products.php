<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $title
 * @property string $description
 * @property integer $price
 * @property integer $inet_price
 * @property double $rating
 * @property integer $rating_description_id
 * @property string $image
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'title', 'description', 'price', 'rating', 'rating_description_id', 'image'], 'required'],
            [['product_id', 'price', 'inet_price', 'rating_description_id'], 'integer'],
            [['description'], 'string'],
            [['rating'], 'number'],
            [['title', 'image'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['rating_description_id'], 'exist', 'skipOnError' => true, 'targetClass' => RatingDescription::className(), 'targetAttribute' => ['rating_description_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'title' => 'Title',
            'description' => 'Description',
            'price' => 'Price',
            'inet_price' => 'Inet Price',
            'rating' => 'Rating',
            'rating_description_id' => 'Rating Description ID',
            'image' => 'Image',
        ];
    }

    public function getRatingDescription()
    {
        return $this->hasOne(RatingDescription::className(), ['id' => 'rating_description_id']);
    }

    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['product_id' => 'id']);
    }
}
