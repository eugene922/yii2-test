<?php

use yii\db\Migration;

class m161005_065640_add_foreign_key_to_products_table extends Migration
{
    public function up()
    {
        $this->addForeignKey(
            'fk-rating_description_id-rating_id',
            'products',
            'rating_description_id',
            'rating_description',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-rating_description_id-rating_id',
            'products'
        );
    }

}
