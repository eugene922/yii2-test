<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m161005_065311_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('products',[
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull()->unique(),
            'description' => $this->text()->notNull(),
            'price' => $this->integer()->notNull(),
            'inet_price' => $this->integer(),
            'rating' => $this->float()->notNull(),
            'rating_description_id' => $this->integer()->notNull(),
            'image' => $this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('products');
    }
}
