<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rating_description`.
 */
class m161005_065350_create_rating_description_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rating_description', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->text()->notNull(),
        ]);

        $this->insert('rating_description', [
            'name' => 'плохой',
            'description' => 'рейтинг <= 3'
        ]);
        $this->insert('rating_description', [
            'name' => 'хороший',
            'description' => '3 < рейтинг <= 4'
        ]);
        $this->insert('rating_description', [
            'name' => 'отличный',
            'description' => '4 < рейтинг <= 5'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rating_description');
    }
}
