<?php

use yii\db\Migration;
use app\components\parser\ConsoleParserHelper;
//use helpers\parser\ConsoleParserHelper;

class m161005_085127_add_data_to_products extends Migration
{
    public function safeUp()
    {
        $file = __DIR__ . '/data/products.xml';

        $parser = new ConsoleParserHelper();

        $parser->
            setMigrationInstanse($this)->
            setFile($file)->
            run();
    }

    public function safeDown()
    {
        return true;
    }

}
