<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reviews`.
 */
class m161006_045430_create_reviews_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'message' => $this->string()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'ip' => $this->string()->notNull(),
            'browser' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reviews');
    }
}
