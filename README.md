Yii 2 test project
============================

run that console command:
    yii migrate

FILES WAS ADDED TO DIRECTORY STRUCTURE
-------------------

        components/
            parser/                         contains class parsers

                ConsoleParserHelper.php     Realize a convenient console output
                ParserHelper.php            Abstract parser, contains all logic for parsing a xml file
                WebParserHelper.php         Insert command without output
        controllers/
            ProductController.php
            ReviewController.php
        migrations/       add migrations for creating products, reviews and rating_description tables; foreign keys and parsing data from migrations/data/products.xml

        models/             add Products.php, Review.php, UploadForm.php, ReviewForm.php
        views/              add product/show.php


FILES WAS MODIFIED
-------------------

        assets/AppAsset.php            add js script
        config/
            web.php       add url routes
        views/site/index.php
